--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--Galil maverick
TFA.AddFireSound( "GalilCraft.Fire", "weapons/tfa_cso/galilcraft/fire.wav", false, "^" )
TFA.AddWeaponSound( "GalilCraft.ClipIn", "weapons/tfa_cso/galilcraft/clipin.wav")
TFA.AddWeaponSound( "GalilCraft.ClipOut", "weapons/tfa_cso/galilcraft/clipout.wav")
TFA.AddWeaponSound( "GalilCraft.Boltpull", "weapons/tfa_cso/galilcraft/boltpull.wav")

--Hunter Killer X-12
TFA.AddFireSound( "X-12.Fire", "weapons/tfa_cso/x-12/fire.wav", false, "^" )
TFA.AddWeaponSound( "X-12.Draw", "weapons/tfa_cso/x-12/draw.wav" )
TFA.AddWeaponSound( "X-12.ClipIn1", "weapons/tfa_cso/x-12/clipin1.wav" )
TFA.AddWeaponSound( "X-12.ClipIn2", "weapons/tfa_cso/x-12/clipin2.wav" )
TFA.AddWeaponSound( "X-12.ClipOut1", "weapons/tfa_cso/x-12/clipout1.wav" )
TFA.AddWeaponSound( "X-12.ClipOut2", "weapons/tfa_cso/x-12/clipout2.wav" )


//StarChaser AR. Shooting AUG

TFA.AddFireSound( "StarAR.Fire2", "weapons/tfa_cso/starchaserar/fire2.wav", false, "^" )

local soundData = {
    name        = "StarAR.Idle" ,
    channel     = CHAN_WEAPON,
    volume      = 1,
    soundlevel  = 80,
    pitchstart  = 100,
    pitchend    = 100,
    sound       = "weapons/tfa_cso/starchaserar/idle.wav"
}

sound.Add(soundData)

--Electron-V
TFA.AddFireSound( "ElectronV.Fire", "weapons/tfa_cso/electronv/fire.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Fire2", "weapons/tfa_cso/electronv/fire2.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Fire3", "weapons/tfa_cso/electronv/fire3.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Exp2", "weapons/tfa_cso/electronv/exp2.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Exp3", "weapons/tfa_cso/electronv/exp3.wav", false, "^" )
TFA.AddWeaponSound( "ElectronV.Draw", "weapons/tfa_cso/electronv/draw.wav" )
TFA.AddWeaponSound( "ElectronV.Idle1", "weapons/tfa_cso/electronv/idle1.wav" )
TFA.AddWeaponSound( "ElectronV.Idle2", "weapons/tfa_cso/electronv/idle2.wav" )
TFA.AddWeaponSound( "ElectronV.Idle3", "weapons/tfa_cso/electronv/idle3.wav" )
TFA.AddWeaponSound( "ElectronV.ClipIn", "weapons/tfa_cso/electronv/clipin.wav" )
TFA.AddWeaponSound( "ElectronV.ClipOut", "weapons/tfa_cso/electronv/clipout.wav" )
