ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "XTRACKER Rocket"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false


ENT.MyModel = "models/weapons/tfa_cso/w_shell_svdex.mdl"
ENT.MyModelScale = 0.15
ENT.Damage = 100
ENT.Radius = 128
if SERVER then

	AddCSLuaFile()

	function ENT:Initialize()

		local model = self.MyModel and self.MyModel or "models/weapons/tfa_cso/w_shell_svdex.mdl"
		
		self.Class = self:GetClass()
		
		self:SetModel(model)
		util.SpriteTrail(self, 0, Color(255,255,255), false, 7, 1, 0.25, 0.125, "tracers/xtracker_trail.vmt")
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(true)
		self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
		self:SetHealth(1)
		self:SetModelScale(self.MyModelScale,0)
		
		local phys = self:GetPhysicsObject()
		
		if (phys:IsValid()) then
			phys:Wake()
			phys:SetMass(1)
			phys:EnableDrag(false)
			phys:EnableGravity(false)
			phys:SetBuoyancyRatio(0)
		end
	end

	function ENT:Think()
		for k, v in pairs(ents.FindInSphere( self.Entity:GetPos(), 320 )) do
			if v:IsPlayer() || v:IsNPC() then
				if v == self.Owner then return end
				self.Entity:GetPhysicsObject():SetVelocity((v:GetPos() - self.Entity:GetPos()) * 2500)
			end
		end
	end

	function ENT:PhysicsCollide(data, physobj)
		local owent = self.Owner and self.Owner or self
		util.BlastDamage(self,owent,self:GetPos(),self.Radius,self.Damage)
		local fx = EffectData()
		fx:SetOrigin(self:GetPos())
		fx:SetNormal(data.HitNormal)
		util.Effect("exp_xtracker",fx)
		self:Remove()
	end
end

if CLIENT then
	
	function ENT:Draw()
		self:DrawModel()
	end

end